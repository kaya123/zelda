import Group from './Group';
// const {ccclass, property} = cc._decorator;

// @ccclass
export default class Sprite extends cc.Node {

    rect : cc.Rect
    hitbox : cc.Rect

    debugSprite : cc.Node

    groups : Group[]

    constructor(groups) {
        super()
        this.groups = groups
    }

    update(dt) {
        
    }

    kill() {
        for(let group of this.groups) {
            group.removeNode(this)
        }
        this.removeFromParent()
    }

}
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameEvent from "./GameEvent"

export class ResManager {

    static _instance = null
    
    res = {}
    
    loadList = [
        {loadType : "dir", type : cc.TextAsset, key:"map", path : "map"},
        {loadType : "dir", type : cc.SpriteFrame, key:"grass", path : "graphics/grass"},
        {loadType : "dir", type : cc.SpriteFrame, key:"objects", path : "graphics/objects"},
        {loadType : "dir", type : cc.SpriteFrame, key:"tilemap", path : "graphics/tilemap"},
        {loadType : "dir", type : cc.SpriteFrame, key:"test", path : "graphics/test"},

        // player anim
        {loadType : "dir", type : cc.SpriteFrame, key:"down", path : "graphics/player/down"},
        {loadType : "dir", type : cc.SpriteFrame, key:"up", path : "graphics/player/up"},
        {loadType : "dir", type : cc.SpriteFrame, key:"right", path : "graphics/player/right"},
        {loadType : "dir", type : cc.SpriteFrame, key:"left", path : "graphics/player/left"},
        {loadType : "dir", type : cc.SpriteFrame, key:"down_attack", path : "graphics/player/down_attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"up_attack", path : "graphics/player/up_attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"left_attack", path : "graphics/player/left_attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"right_attack", path : "graphics/player/right_attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"down_idle", path : "graphics/player/down_idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"up_idle", path : "graphics/player/up_idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"left_idle", path : "graphics/player/left_idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"right_idle", path : "graphics/player/right_idle"},
        
        // monster
        {loadType : "dir", type : cc.SpriteFrame, key:"bamboo-attack", path : "graphics/monsters/bamboo/attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"bamboo-idle", path : "graphics/monsters/bamboo/idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"bamboo-move", path : "graphics/monsters/bamboo/move"},
        {loadType : "dir", type : cc.SpriteFrame, key:"raccoon-attack", path : "graphics/monsters/raccoon/attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"raccoon-idle", path : "graphics/monsters/raccoon/idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"raccoon-move", path : "graphics/monsters/raccoon/move"},      
        {loadType : "dir", type : cc.SpriteFrame, key:"spirit-attack", path : "graphics/monsters/spirit/attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"spirit-idle", path : "graphics/monsters/spirit/idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"spirit-move", path : "graphics/monsters/spirit/move"},  
        {loadType : "dir", type : cc.SpriteFrame, key:"squid-attack", path : "graphics/monsters/squid/attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"squid-idle", path : "graphics/monsters/squid/idle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"squid-move", path : "graphics/monsters/squid/move"},         
        
        // weapon
        {loadType : "dir", type : cc.SpriteFrame, key:"axe", path : "graphics/weapons/axe"},         
        {loadType : "dir", type : cc.SpriteFrame, key:"lance", path : "graphics/weapons/lance"},         
        {loadType : "dir", type : cc.SpriteFrame, key:"rapier", path : "graphics/weapons/rapier"},         
        {loadType : "dir", type : cc.SpriteFrame, key:"sai", path : "graphics/weapons/sai"},         
        {loadType : "dir", type : cc.SpriteFrame, key:"sword", path : "graphics/weapons/sword"},      
        
        // magic
        {loadType : "dir", type : cc.SpriteFrame, key:"flame", path : "graphics/particles/flame/frames"},
        {loadType : "dir", type : cc.SpriteFrame, key:"aura", path : "graphics/particles/aura"},
        {loadType : "dir", type : cc.SpriteFrame, key:"heal", path : "graphics/particles/heal/frames"},
        {loadType : "file", type : cc.SpriteFrame, key:"flame_icon", path : "graphics/particles/flame/fire"},
        {loadType : "file", type : cc.SpriteFrame, key:"heal_icon", path : "graphics/particles/heal/heal"},
        
        // attacks 
        {loadType : "dir", type : cc.SpriteFrame, key:"claw", path : "graphics/particles/claw"},
        {loadType : "dir", type : cc.SpriteFrame, key:"slash", path : "graphics/particles/slash"},
        {loadType : "dir", type : cc.SpriteFrame, key:"sparkle", path : "graphics/particles/sparkle"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf_attack", path : "graphics/particles/leaf_attack"},
        {loadType : "dir", type : cc.SpriteFrame, key:"thunder", path : "graphics/particles/thunder"},
        
        // monster deaths
        {loadType : "dir", type : cc.SpriteFrame, key:"squid", path : "graphics/particles/smoke_orange"},
        {loadType : "dir", type : cc.SpriteFrame, key:"raccoon", path : "graphics/particles/raccoon"},
        {loadType : "dir", type : cc.SpriteFrame, key:"spirit", path : "graphics/particles/nova"},
        {loadType : "dir", type : cc.SpriteFrame, key:"bamboo", path : "graphics/particles/bamboo"},
        
        // leafs 
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf1", path : "graphics/particles/leaf1"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf2", path : "graphics/particles/leaf2"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf3", path : "graphics/particles/leaf3"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf4", path : "graphics/particles/leaf4"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf5", path : "graphics/particles/leaf5"},
        {loadType : "dir", type : cc.SpriteFrame, key:"leaf6", path : "graphics/particles/leaf6"},

        // font
        {loadType : "file", type : cc.Font, key:"font", path : "graphics/font/joystix"},
        
        // audio
        {loadType : "file", type : cc.AudioClip, key:"audio_claw", path : "audio/attack/claw"},
        {loadType : "file", type : cc.AudioClip, key:"audio_fireball", path : "audio/attack/fireball"},
        {loadType : "file", type : cc.AudioClip, key:"audio_slash", path : "audio/attack/slash"},
        {loadType : "file", type : cc.AudioClip, key:"audio_fire", path : "audio/Fire"},
        {loadType : "file", type : cc.AudioClip, key:"audio_heal", path : "audio/heal"},
        {loadType : "file", type : cc.AudioClip, key:"audio_hit", path : "audio/hit"},
        {loadType : "file", type : cc.AudioClip, key:"audio_main", path : "audio/main"},
        {loadType : "file", type : cc.AudioClip, key:"audio_sword", path : "audio/sword"},
        {loadType : "file", type : cc.AudioClip, key:"audio_death", path : "audio/death"},

    ]
    
    loadCount = 0

    private constructor() {

    }

    static getInstance() : ResManager {
        if(this._instance == null) {
            this._instance = new ResManager()
        }
        return this._instance
    }

    load() {
        this.loadCount = 0
        this.loadRes()
    }
    
    loadRes () {

        if(this.loadCount == this.loadList.length) {
            cc.director.emit(GameEvent.loadResEnd)
            return
        }

        let resInfo = this.loadList[this.loadCount]

        if(resInfo.loadType == "dir") {
            cc.resources.loadDir(resInfo.path, resInfo.type, null, (err, assets)=>{

                let array = {}
                for(let asset of assets) {
                    array[asset["_name"]] = asset
                }

                this.res[resInfo.key] = array

                this.loadCount += 1
                this.loadRes()
            })
        } else if(resInfo.loadType == "file") {
            cc.resources.load(resInfo.path, resInfo.type, null, (err, asset)=>{

                this.res[resInfo.key] = asset

                this.loadCount += 1
                this.loadRes()
            })
        }
    }

    getRes() {
        return this.res
    }

}

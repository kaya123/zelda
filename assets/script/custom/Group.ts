import Sprite from './Sprite';
const {ccclass, property} = cc._decorator;

@ccclass
export default class Group extends cc.Component {

    childList : Sprite[] = []

    addSprite(child : Sprite) {
        this.childList.push(child)
    }

    getSprites() : Sprite[] {
        return this.childList
    }
    
    update(dt) {
        for(let child of this.childList) {
            child.update(dt)
        }
    }

    removeNode(node : cc.Node) {
        let findIndex = -1
        for(let i=0; i<this.childList.length; i++) {
            if(this.childList[i] == node) {
                findIndex = i
                break
            }
        }

        if(findIndex != -1) {
            this.childList.splice(findIndex, 1)
        }

    }

}
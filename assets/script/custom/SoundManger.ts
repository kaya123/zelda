import { ResManager } from './ResManager';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

export default class SoundManager {

    static _instance : SoundManager = null

    static getInstance() : SoundManager {
        if(this._instance == null) {
            this._instance = new SoundManager()
        }
        return this._instance
    }

    playMusic(name) {
        let key = "audio_" + name
        let music = ResManager.getInstance().getRes()[key]
        cc.audioEngine.playMusic(music, true)
    }

    playEffect(name) {
        let key = "audio_" + name
        let effect = ResManager.getInstance().getRes()[key]
        cc.audioEngine.playEffect(effect, false)
    }

}

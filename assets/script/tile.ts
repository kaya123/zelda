// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

import Sprite from "./custom/Sprite";
import Global from "./settings";
import { changePos, getColorNode, rectInflate } from './support';


let HITBOX_OFFSET = Global.HITBOX_OFFSET
let TILESIZE = Global.TILESIZE

export default class Tile extends Sprite {

    sprite_type : string = null
    image : cc.Node = null
    // rect = null
    // hitbox = null


	constructor(pos,groups,sprite_type,surface = null) {

        super(groups)

        this.sprite_type = sprite_type


        pos = changePos(pos)
        
        // console.log("tile pos =", pos[0], pos[1])

        let width = TILESIZE
        let height = TILESIZE
        if(surface == null) {
            surface = this
            this.setContentSize(width, height)
        } else {
            let node = this
            let sp = node.addComponent(cc.Sprite)
            sp.spriteFrame = surface
            width = sp.spriteFrame.getTexture().width
            height = sp.spriteFrame.getTexture().height
            surface = node
        }
        surface.script = this

		let y_offset = HITBOX_OFFSET[sprite_type]
		this.image = surface

        // pygame的节点起点是左上角
		if(sprite_type == 'object') {
            this.image.setPosition(pos[0]+width/2, pos[1]-height/2+TILESIZE)
        }
		else {
            this.rect = this.image.getBoundingBox()
            this.image.setPosition(pos[0]+width/2, pos[1]-height/2)
        }
        
        this.rect = this.image.getBoundingBox()
        this.hitbox = rectInflate(this.rect, 0, y_offset)

        for(let item of groups) {
            item.addSprite(surface)
        }
    }


    start () {

    }

}



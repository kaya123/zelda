import Sprite from './custom/Sprite';
import { changePos, import_folder, choice, randomInt, changeToList } from './support';
// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html


export default class AnimationPlayer {

    frames = {
        // magic
        'flame': this.getResList('flame'),
        'aura': this.getResList('aura'),
        'heal': this.getResList('heal'),
        
        // attacks 
        'claw': this.getResList('claw'),
        'slash': this.getResList('slash'),
        'sparkle': this.getResList('sparkle'),
        'leaf_attack': this.getResList('leaf_attack'),
        'thunder': this.getResList('thunder'),

        // monster deaths
        'squid': this.getResList('squid'),
        'raccoon': this.getResList('raccoon'),
        'spirit': this.getResList('spirit'),
        'bamboo': this.getResList('bamboo'),
        
        // leafs 
        'leaf': [
            this.getResList('leaf1'),
            this.getResList('leaf2'),
            this.getResList('leaf3'),
            this.getResList('leaf4'),
            this.getResList('leaf5'),
            this.getResList('leaf6'),
            this.reflect_images(this.getResList('leaf1')),
            this.reflect_images(this.getResList('leaf2')),
            this.reflect_images(this.getResList('leaf3')),
            this.reflect_images(this.getResList('leaf4')),
            this.reflect_images(this.getResList('leaf5')),
            this.reflect_images(this.getResList('leaf6'))
            ]
        }

    constructor() {
    }

    getResList(name) {
        let dict = import_folder(name)
        return changeToList(dict)
    }

    reflect_images(frames) {
		let new_frames = []

		for(let key in frames) {
            let frame = frames[key]
            let flipped_frame = frame.clone()
            flipped_frame.isFlip = true // 标记翻转
            new_frames.push(flipped_frame)
        }
		return new_frames
    }

    create_grass_particles(pos,groups) {
        let randomIndex = randomInt(this.frames['leaf'].length)
        let animation_frames = this.frames['leaf'][randomIndex]
        new ParticleEffect(pos,animation_frames,groups)
    }

    create_particles(animation_type,pos,groups) {
        let animation_frames = this.frames[animation_type]
		new ParticleEffect(pos,animation_frames,groups)
    }

}


class ParticleEffect extends Sprite {

    sprite_type
    frame_index
    animation_speed
    frames
    image : cc.Node

    constructor(pos,animation_frames,groups) {
        super(groups)

        // pos = changePos(pos)

        this.sprite_type = 'magic'
		this.frame_index = 0
		this.animation_speed = 0.15
		this.frames = animation_frames

        this.image = this
        let sp = this.addComponent(cc.Sprite)
        sp.sizeMode = cc.Sprite.SizeMode.RAW
        sp.trim = false
        sp.spriteFrame = this.frames[this.frame_index]

        this.image.setPosition(pos[0], pos[1])
        this.rect = this.image.getBoundingBox()
        this.hitbox = this.rect

        for(let item of groups) {
            item.addSprite(this.image)
        }
        
    }

    animate() {
        this.frame_index += this.animation_speed
        if(this.frame_index >= this.frames.length){
            this.kill()
        }else {
            let spriteFrame = this.frames[Math.floor(this.frame_index)]
            this.getComponent(cc.Sprite).spriteFrame = spriteFrame
            if(spriteFrame.isFlip) {
                this.scaleX = -1
            } else {
                this.scaleX = 1
            }
        }
    }

    update() {
        this.animate()
    }

}